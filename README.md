Responsive Google Adsense
=========================

Include:
```
<script src="mzy.responsive-ads.js"></script>
```

Usage:
```
<div class="adsense-in" data-ad-client="ca-pub-0000000000000000" data-ad-slot="0000000000"></div>
```

Credits
-------

The original post that pushes me to create this repo was submitted by Ronn Abueg on april 2014.

Making Your Google AdSense Ads Really Responsive:
http://abueg.org/posts/making-your-google-adsense-ads-really-responsive